let number = Number(prompt('Give a number: '));

console.log('The number you provided is ' + number + '.');


for (let i = number ; i >= 0; i--) {

	if (i <= 50) {
		console.log('The current value is at 50. terminating the loop.')
		break;
	}

	if (i % 10 === 0) {
		console.log('The number is divisible by 10. Skipping the number.');
		continue;
	} 

	if (i % 5 === 0) {
		console.log(i);
	}
}

let string = 'supercalifragilisticexpialidocious';
let filteredString = '';

console.log(string);

for (let i = 0; i < string.length; i++) {

	if (	string[i].toLowerCase() === "a" ||
			string[i].toLowerCase() === "i" ||
			string[i].toLowerCase() === "u" ||
			string[i].toLowerCase() === "e" ||
			string[i].toLowerCase() === "o"

		) {
		continue;
	} else {
		filteredString += string[i];	
	}
}
console.log(filteredString);

